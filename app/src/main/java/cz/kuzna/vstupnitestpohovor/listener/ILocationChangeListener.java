package cz.kuzna.vstupnitestpohovor.listener;

import android.location.Location;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/24/2015 6:46 PM
 */
public interface ILocationChangeListener {

    void onLocationChanged(final Location location);
}