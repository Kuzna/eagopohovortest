package cz.kuzna.vstupnitestpohovor.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import cz.kuzna.vstupnitestpohovor.listener.ILocationChangeListener;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/24/2015 4:32 PM
 */
public class LocationReceiver extends BroadcastReceiver {

    public static final String BUNDLE_LOCATION_KEY = "locKey";

    private ILocationChangeListener locationListener;

    public LocationReceiver(final ILocationChangeListener locationListener) {
        this.locationListener = locationListener;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final Location location = (Location)intent.getExtras().get(BUNDLE_LOCATION_KEY);

        if (locationListener != null && location != null) {
            locationListener.onLocationChanged(location);
        }
    }
}