package cz.kuzna.vstupnitestpohovor.util;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/25/2015 3:55 PM
 */
public class GoogleGeocoderUtil {

    private static final String TAG = "GoogleGeocoderUtil";

    private static final String ATTR_RESULTS = "results";
    private static final String ATTR_STATUS = "status";
    private static final String ATTR_TYPES = "types";
    private static final String ATTR_STREET_ADDRESS = "street_address";
    private static final String ATTR_FORMATTED_ADDRESS = "formatted_address";

    private static final String STATUS_OK = "OK";

    private static final String GOOGLE_GEOCODER_API = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";

    /**
     *  Get address via Google Geocoder API .
     *
     * @param latitude
     * @param longtitude
     * @return
     * @throws IOException
     */
    public static String getFromLocation(final double latitude, final double longtitude) throws IOException {
        try {
            final HttpClient httpClient = new DefaultHttpClient();
            final HttpGet httpGet = new HttpGet(geQuery(latitude, longtitude));
            httpGet.addHeader("Accept-Language", "cs-CZ");

            Log.d(TAG,"Calling -> ".concat(httpGet.getURI().toString()));

            final HttpResponse httpResponse = httpClient.execute(httpGet);
            final String jsonString = EntityUtils.toString(httpResponse.getEntity());

            return parseResponse(jsonString);
        } catch (IOException | JSONException e) {
            return e.getLocalizedMessage();
        }
    }

    /**
     * Parse Google Geocoder API response.
     * <p>
     * Status codes: https://developers.google.com/maps/documentation/geocoding/#StatusCodes
     * <p/>
     * @param jsonString
     * @return
     * @throws JSONException
     */
    public static String parseResponse(final String jsonString) throws JSONException {
        final JSONObject jsonResponse = new JSONObject(jsonString);

        if(jsonResponse.has(ATTR_STATUS) && jsonResponse.getString(ATTR_STATUS).equals(STATUS_OK)) {
            final JSONArray results = jsonResponse.getJSONArray(ATTR_RESULTS);

            for(int i = 0; i < results.length(); i++) {
                final JSONObject result = results.getJSONObject(i);
                final JSONArray types = result.getJSONArray(ATTR_TYPES);

                for(int j = 0; j < types.length(); j++) {
                    final String type = types.getString(j);
                    if(type != null && type.equals(ATTR_STREET_ADDRESS)) {
                        final String address = result.getString(ATTR_FORMATTED_ADDRESS);
                        return address;
                    }
                }
            }
            Log.e(TAG, "No results in Google API Geocoder response!");
        } else {
            Log.e(TAG, jsonResponse.has(ATTR_STATUS) ? jsonResponse.getString(ATTR_STATUS) : "Empty response");
        }

        return "";
    }

    /**
     * Return query of Google Geocoder API filled with langtitude and longtitude.
     *
     * @param latitude - Latitude of location
     * @param longtitude - Longtitude of location
     * @return
     */
    public static String geQuery(final double latitude, final double longtitude) {
        final StringBuilder sb = new StringBuilder();
        sb.append(GOOGLE_GEOCODER_API);
        sb.append(Double.toString(latitude) + "," + Double.toString(longtitude));

        return sb.toString();
    }
}