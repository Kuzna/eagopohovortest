package cz.kuzna.vstupnitestpohovor.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import cz.kuzna.vstupnitestpohovor.R;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/25/2015 8:03 PM
 *
 * Base activity for handling back pressed event.
 */
public class BackPressedTwiceActivity extends Activity {

    private static final int BACK_PRESSED_TIMER = 2000;

    private boolean backPressed = false;
    private Toast   toast       = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if (this.backPressed) {
            super.onBackPressed();
        } else {
            this.backPressed = true;
            showToast(getString(R.string.msgBackPressed));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressed = false;
                }
            }, BACK_PRESSED_TIMER);
        }
    }

    private void showToast(String message) {
        if (this.toast == null || this.toast.getView() == null) {
            this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        } else {
            this.toast.setText(message);
        }

        this.toast.show();
    }

    @Override
    protected void onPause() {
        if (this.toast != null) {
            this.toast.cancel();
        }
        super.onPause();
    }
}