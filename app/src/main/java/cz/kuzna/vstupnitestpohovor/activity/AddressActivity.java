package cz.kuzna.vstupnitestpohovor.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import cz.kuzna.vstupnitestpohovor.R;
import cz.kuzna.vstupnitestpohovor.listener.ILocationChangeListener;
import cz.kuzna.vstupnitestpohovor.receiver.LocationReceiver;
import cz.kuzna.vstupnitestpohovor.service.MainService;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/24/2015 8:03 PM
 *
 * AddressActivity bude mít dvě textové pole (poloha a adresa) a tlačítko "Zjisti adresu". Textové pole poloha se bude aktualizovat při přijetí každého broadcastu.
 * Po kliknutí na tlačítko activity zavolá metodu služby getActualAddress a zjistí adresu, kterou aktualizuje pole adresa.
 * Po kliknutí na tlačítko zpět se zobrazí toast "pro ukončení zmáčkněte opět tlačítko zpět". Po opětovném kliknutí se ukončí služba MainService a AddressActivity.
 */
public class AddressActivity extends Activity implements ILocationChangeListener {

    public static final String BROADCAST_LOCATION = "location.broadcast";

    private LocationReceiver locationReceiver;

    private TextView textViewLocation;

    private TextView textViewAddress;

    private MainServiceConnection mainServiceConnection;

    private MainService service;

    private boolean isBound = false;

    private static final int BACK_PRESSED_TIMER = 2000;

    private boolean backPressed = false;

    private Toast   toast       = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        this.textViewLocation = (TextView) findViewById(R.id.textViewLocation);
        this.textViewAddress = (TextView) findViewById(R.id.textViewAddress);

        this.locationReceiver = new LocationReceiver(this);
        this.mainServiceConnection = new MainServiceConnection();
    }

    @Override
    protected void onStart() {
        super.onStart();
        final Intent intent = new Intent(this, MainService.class);
        bindService(intent, mainServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isBound) {
            unbindService(mainServiceConnection);
            isBound = false;
        }
    }

    public void clickGetAddress(final View view) {
        if (isBound) {
            this.textViewAddress.setText(service.getActualAddress());
        }
    }

    @Override
    public void onBackPressed() {
        if (this.backPressed) {
            stopService(new Intent(getApplicationContext(), MainService.class));
            super.onBackPressed();
        } else {
            this.backPressed = true;
            showToast(getString(R.string.msgBackPressed));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressed = false;
                }
            }, BACK_PRESSED_TIMER);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(this.locationReceiver, new IntentFilter(AddressActivity.BROADCAST_LOCATION));
    }

    @Override
    public void onPause()
    {
        unregisterReceiver(this.locationReceiver);
        if (this.toast != null) {
            this.toast.cancel();
        }
        super.onPause();
    }

    @Override
    public void onLocationChanged(final Location location) {
        Log.d(getClass().getName(),"Location to refresh: ".concat(location.getLatitude() + "," + location.getLongitude()));
        this.textViewLocation.setText(location.getLongitude() + ", " + location.getLatitude());
    }

    private void showToast(final String message) {
        if (this.toast == null || this.toast.getView() == null) {
            this.toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        } else {
            this.toast.setText(message);
        }

        this.toast.show();
    }

    private class MainServiceConnection implements android.content.ServiceConnection {

        public void onServiceConnected(final ComponentName className, final IBinder binder) {
            final MainService.MainServiceBinder mainServiceBinder = (MainService.MainServiceBinder) binder;
            service = mainServiceBinder.getService();
            isBound = true;
        }

        public void onServiceDisconnected(final ComponentName className) {
            isBound = false;
        }
    };
}