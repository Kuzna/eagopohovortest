package cz.kuzna.vstupnitestpohovor.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import cz.kuzna.vstupnitestpohovor.R;
import cz.kuzna.vstupnitestpohovor.service.MainService;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/24/2015 8:03 PM
 *
 * SplashActivity se spustí po startu. Po spuštění spustí službu MainService.
 * Po pěti sekundách se spustí AddressActivity. SplashActivity se ukončí.
 * Tuto activitu nelze uživatelsky ukončit. Uprostřed activity bude logo Eago systems (například z mého podpisu) a pod tím se bude zobrazovat progressbar.
 */
public class SplashActivity extends Activity{

    private static final short  TIMEOUT = 5; // 5 second

    private ProgressBar         progressBar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);

        startService(new Intent(getApplicationContext(), MainService.class));

        new ProgressTask(TIMEOUT).execute();
    }

    @Override
    public void onBackPressed() {
    }

    private class ProgressTask extends AsyncTask<Void, Integer, Void> {

        private static final int PROGRESS_BAR_PART = 20;

        private final int interval;
        private final int timeout;

        /**
         * Create the progress task.
         *
         * @param timeout in seconds
         */
        public ProgressTask(final int timeout) {
            this.timeout = timeout;
            this.interval = timeout * 1000 /  PROGRESS_BAR_PART;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            final int part = 100 / PROGRESS_BAR_PART;

            for (int i = 0; i < PROGRESS_BAR_PART; i++) {
                publishProgress((part * i) + part);

                try {
                    Thread.sleep(interval);
                } catch (Exception e) {
                    Log.e(getClass().getName(), e.getMessage());
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(final Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(final Void values) {
            super.onPostExecute(values);

            startAddressActivity();
            finish();
        }
    }

    /**
     * Start address activity.
     */
    private void startAddressActivity() {
        final Intent intent = new Intent(getApplicationContext(), AddressActivity.class);
        startActivity(intent);
    }
}
