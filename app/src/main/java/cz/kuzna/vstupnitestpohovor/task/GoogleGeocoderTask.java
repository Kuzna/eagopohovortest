package cz.kuzna.vstupnitestpohovor.task;

import android.location.Location;
import android.os.AsyncTask;

import java.io.IOException;

import cz.kuzna.vstupnitestpohovor.util.GoogleGeocoderUtil;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/25/2015 4:44 PM
 */
public class GoogleGeocoderTask extends AsyncTask<Location, Void, String> {

    @Override
    protected String doInBackground(Location... params) {
        try {
            return GoogleGeocoderUtil.getFromLocation(params[0].getLatitude(), params[0].getLongitude());
        } catch (IOException e) {
            return e.getLocalizedMessage();
        }
    }
}