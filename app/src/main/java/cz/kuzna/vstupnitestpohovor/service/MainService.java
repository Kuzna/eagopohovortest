package cz.kuzna.vstupnitestpohovor.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.ExecutionException;

import cz.kuzna.vstupnitestpohovor.R;
import cz.kuzna.vstupnitestpohovor.activity.AddressActivity;
import cz.kuzna.vstupnitestpohovor.receiver.LocationReceiver;
import cz.kuzna.vstupnitestpohovor.task.GoogleGeocoderTask;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/24/2015 8:03 PM
 *
 * Služba MainService bude mít jednu globální proměnnou actualLocation, jednu veřejnou metodu getActualAddress.
 * Po startu se zahájí zjišťování polohy (z GPS i Networku). Při získání nové polohy se tato uloží do globální proměnné a současně se odešle broadcastem.
 * Metoda getActualAddress použije hodnotu actualLocation a zjistí přes Google textovou adresu - (stačí zřetězit výsledek z google), kterou vrátí.
 * Pokud bude actualLocation null, tak vrátí řetězec "Nenalezena poloha".
 */
public class MainService extends Service implements LocationListener {

    private static final int    MIN_DISTANCE = 0;
    private static final int    MIN_TIME = 1000;

    private LocationManager     locationManager     = null;

    private Location            actualLocation;

    private MainServiceBinder   binder = new MainServiceBinder();

    @Override
    public IBinder onBind(final Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        Log.d(getClass().getName(), "Starting main service...");

        locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if(locationManager != null) {
            locationManager.removeUpdates(this);
            locationManager = null;
        }
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(final Location location) {
        actualLocation = location;

        final Intent intent = new Intent();
        intent.setAction(AddressActivity.BROADCAST_LOCATION);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.putExtra(LocationReceiver.BUNDLE_LOCATION_KEY, actualLocation);
        sendBroadcast(intent);
    }

    @Override
    public void onStatusChanged(final String provider, final int status, final Bundle extras) {

    }

    @Override
    public void onProviderEnabled(final String provider) {

    }

    @Override
    public void onProviderDisabled(final String provider) {

    }

    public String getActualAddress() {
        if(actualLocation == null) {
            return getString(R.string.msgLocationNotFound);
        } else {
            try {
                final GoogleGeocoderTask task = new GoogleGeocoderTask();
                task.execute(actualLocation);
                return task.get();
            } catch (InterruptedException | ExecutionException e) {
                Log.e(getClass().getName(),"",e);
                return "";
            }
        }
    }

    public class MainServiceBinder extends Binder {
        public MainService getService() {
            return MainService.this;
        }
    }
}
