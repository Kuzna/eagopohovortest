package cz.kuzna.vstupnitestpohovor.activity;

import android.test.ActivityUnitTestCase;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/26/2015 6:50 PM
 */
public class SplashActivityTest extends ActivityUnitTestCase<SplashActivity> {

    public SplashActivityTest(Class<SplashActivity> activityClass) {
        super(activityClass);
    }
}