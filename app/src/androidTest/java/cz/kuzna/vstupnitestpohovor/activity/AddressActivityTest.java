package cz.kuzna.vstupnitestpohovor.activity;

import android.test.ActivityUnitTestCase;

/**
 * Created by Radek Kuznik (Radek.kuznik1@gmail.com)
 *
 * @Date 1/26/2015 6:50 PM
 */
public class AddressActivityTest extends ActivityUnitTestCase<AddressActivity> {

    public AddressActivityTest(Class<AddressActivity> activityClass) {
        super(activityClass);
    }
}